/**
 * @file    read_temp.cpp
 * @author  Paul Thomas
 * @date    6/11/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "../common.h"
#include "../sender.h"

#include "strato_test_interface.h"

#include <cstdio>

static const char *
channel2str(strato_test_interface_read_temp_channel_e channel) {
  switch (channel) {
  case STRTC_TOP:
    return "TOP";
  case STRTC_MIDDLE:
    return "MIDDLE";
  case STRTC_BOTTOM:
    return "BOTTOM";
  default:
    return "UNKNOWN";
  }
}

extern "C" void strato_test_interface_request_on_read_temp(
    const strato_test_interface_read_temp_request_payload_t *request_payload) {
  printf("Received read temp. channel: %s\r\n",
         channel2str(request_payload->channel));
  strato_test_interface_read_temp_reply_payload_t reply = {
      .channel = request_payload->channel,
      .value = 3.141592,
  };
  send(STC_READ_TEMP, &reply);
}
