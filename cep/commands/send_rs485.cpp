/**
 * @file    send_rs485.cpp
 * @author  Paul Thomas
 * @date    6/11/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "../common.h"
#include "../sender.h"

#include "strato_test_interface.h"

#include <cstdio>

extern "C" void strato_test_interface_request_on_send_rs485(
    const strato_test_interface_send_rs485_request_payload_t *request_payload) {
  printf("Received send RS485. index: %u, length: %u\r\n, delay: %lu\r\n",
         request_payload->index, request_payload->length,
         request_payload->delay_ms);
  strato_test_interface_send_rs485_reply_payload_t reply = {
      .index = request_payload->index,
      .result =
          strato_test_interface_send_rs485_reply_payload_t::STTX_RS485_SUCCESS};
  send(STC_SEND_RS485, &reply);
}