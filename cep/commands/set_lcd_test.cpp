/**
 * @file    set_lcd_test.cpp
 * @author  Paul Thomas
 * @date    6/11/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "../common.h"
#include "../sender.h"

#include "strato_test_interface.h"

#include <cstdio>

static const char *screen2str(int screen) {
  switch (screen) {
  case strato_test_interface_set_lcd_test_payload_t::STSLT_OFF:
    return "OFF";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_TEST:
    return "TEST";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_BLACK:
    return "BLACK";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_RED:
    return "RED";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_GREEN:
    return "GREEN";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_BLUE:
    return "BLUE";
  case strato_test_interface_set_lcd_test_payload_t::STSLT_WHITE:
    return "WHITE";
  default:
    return "UNKNOWN";
  }
}

extern "C" void strato_test_interface_request_on_set_lcd_test(
    const strato_test_interface_set_lcd_test_payload_t *request_payload) {
  printf("Received set lcd. screen: %s, brightness: %d%%\r\n",
         screen2str(request_payload->screen),
         request_payload->backlight_brightness_pct);
  send(STC_SET_LCD_TEST, &request_payload);
}
