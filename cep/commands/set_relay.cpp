/**
 * @file    set_relay.cpp
 * @author  Paul Thomas
 * @date    6/11/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "../common.h"
#include "../sender.h"

#include "strato_test_interface.h"

#include <cstdio>
extern "C" void strato_test_interface_request_on_set_relay(
    const strato_test_interface_set_relay_payload_t *request_payload) {
  printf("Received set relay. index: %u, state: %s\r\n", request_payload->index,
         request_payload->state ? "CLOSED" : "OPEN");
  send(STC_SET_RELAY, request_payload);
}
