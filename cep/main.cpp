/**
 * @file    main.cpp
 * @author  Paul Thomas
 * @date    6/6/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "main.h"
#include "gpio.h"
#include "heartbeat.h"
#include "receiver.h"
#include "usart.h"

#include <array>
#include <cstdio>

int main() {
  HAL_Init();
  SystemClock_Config();

  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();

  printf("\033c");
  printf("Strato mock device\r\n");

  receiverInit();

  volatile int do_run = 1;
  while (do_run != 0) {
    heartbeatRoutine();
    receiverRoutine();
  }
  return 0;
}