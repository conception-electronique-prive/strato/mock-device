/**
 * @file    receiver.cpp
 * @author  Paul Thomas
 * @date    6/6/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "common.h"
#include "strato_test_interface.h"

#include <array>
#include <cstdio>
#include <serial_message.h>
#include <usart.h>

static uint16_t uartRxCount = 0;
static std::array<uint8_t, BUFFER_CAPACITY> uartRxBuffer = {};
static std::array<uint8_t, BUFFER_CAPACITY> accumulatorBuffer = {};
static std::array<uint8_t, BUFFER_CAPACITY> messageRxBuffer = {};
static serial_message_accumulator_t accumulator;

extern "C" void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart,
                                           uint16_t Size) {
  if (huart == &huart1)
    uartRxCount = Size;
}

static void handlePacketReception() {
  printf("Received packet\r\n");
  uint16_t count = messageRxBuffer.size();
  const auto result = serial_message_decode_message(
      &accumulator, messageRxBuffer.data(), &count);
  if (result != SMCR_SUCCESS || count == 0) {
    printf("Failed to deserialize result, aborting\r\n");
    return;
  }
  printf("Dispatching message\r\n");
  strato_test_interface_dispatcher(messageRxBuffer.data(), count);
}

static void handleUartReception() {

  if (uartRxCount == 0)
    return;

  printf("Received %u bytes\r\n", uartRxCount);
  printf("[");
  for (int i = 0; i < uartRxCount; ++i) {
    if (i != 0)
      printf(", ");
    printf("%02x", uartRxBuffer[i]);
  }
  printf("]\r\n");
  uint16_t count = uartRxCount;
  auto result = serial_message_accumulate_buffer(uartRxBuffer.data(), &count,
                                                 &accumulator);

  if (result == SMAR_NO_MESSAGE) {
    printf("No message received, should have, will reset accumulator\r\n");
  } else if (result == SMAR_HAS_MESSAGE) {
    handlePacketReception();
  } else {
    printf("Something went wrong when accumulating buffer. Error: %d\r\n",
           result);
  }
  serial_message_reset_accumulator(&accumulator);
}

void receiverInit() {
  serial_message_init_accumulator(&accumulator, accumulatorBuffer.data(),
                                  accumulatorBuffer.size());
  HAL_UARTEx_ReceiveToIdle_IT(&huart1, uartRxBuffer.data(),
                              uartRxBuffer.size());
}

void receiverRoutine() {
  if (uartRxCount == 0)
    return;

  handleUartReception();

  uartRxCount = 0;
  HAL_UARTEx_ReceiveToIdle_IT(&huart1, uartRxBuffer.data(),
                              uartRxBuffer.size());
}