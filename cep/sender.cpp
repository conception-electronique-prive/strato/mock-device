/**
 * @file    sender.cpp
 * @author  Paul Thomas
 * @date    6/6/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "sender.h"

#include "common.h"
#include "serial_message.h"
#include <usart.h>

#include <array>
#include <cstdio>

std::array<uint8_t, BUFFER_CAPACITY> messageTxBuffer = {};
std::array<uint8_t, BUFFER_CAPACITY> packetBuffer = {};

void send(strato_test_interface_command_e command) { send(command, nullptr); }

void send(strato_test_interface_command_e command, const void *payload) {
  uint16_t messageLength = messageTxBuffer.size();
  auto messageResult = strato_test_interface_serialize_command(
      command, false, payload, messageTxBuffer.data(), &messageLength);
  if (messageResult != STCR_SUCCESS) {
    printf("Failed to make message. Error: %d\r\n", messageResult);
    return;
  }

  uint16_t packetLength = packetBuffer.size();
  auto packetResult =
      serial_message_make_packet(messageTxBuffer.data(), messageLength,
                                 packetBuffer.data(), &packetLength);
  if (packetResult != SMCR_SUCCESS) {
    printf("Failed to make packet. Error: %d\r\n", packetResult);
    return;
  }
  HAL_UART_Transmit(&huart1, packetBuffer.data(), packetLength, HAL_MAX_DELAY);
}