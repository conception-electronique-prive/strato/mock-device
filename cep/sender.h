/**
 * @file    sender.h
 * @author  Paul Thomas
 * @date    6/6/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#ifndef SENDER_H
#define SENDER_H

#include "common.h"
#include "strato_test_interface.h"

void send(strato_test_interface_command_e command);
void send(strato_test_interface_command_e command, const void *payload);

#endif // SENDER_H
